package com.ashish.ashish_zeta.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetLocationsResponse {
	private List<LocationSuggestions> location_suggestions;

    private String status;

    private int has_more;

    private int has_total;

    private boolean user_has_addresses;
}
