package com.ashish.ashish_zeta.models;

import lombok.Data;

@Data
public class LocationSuggestions {
	private String entity_type;

    private int entity_id;

    private String title;

    private double latitude;

    private double longitude;

    private int city_id;

    private String city_name;

    private int country_id;

    private String country_name;

}
