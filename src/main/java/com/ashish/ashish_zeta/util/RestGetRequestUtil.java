package com.ashish.ashish_zeta.util;

import java.util.Map;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestGetRequestUtil {
	
	public Response httpGetAPICall(String APIUrl, String createRequestPath, Map<String, Object> paramMap,
			Map<String, String> headerMap) {

		RestAssured.baseURI = APIUrl;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.headers(headerMap).params(paramMap).when().get(APIUrl + createRequestPath);

		return response;
	}
}
