package com.ashish.ashish_zeta.util;

import java.util.Properties;


import lombok.Data;

@Data
public class ApplicationPropertiesInitializer {
	private String locationUrl;
	private String getLocationPath;
	private String userKey;
	private Properties properties;
	public ApplicationPropertiesInitializer() {
		ApplicationAutomationProperties applicationAutomationProperties = new ApplicationAutomationProperties(
				"/application.properties", "src/main/resources");
		properties = applicationAutomationProperties.getProperties();
		locationUrl = properties.getProperty("com.test.location.url");
		getLocationPath = properties.getProperty("com.test.getLocationPath");
		userKey = properties.getProperty("com.test.userKey");
	}
}
