package com.ashish.ashish_zeta.util;

import java.util.HashMap;
import java.util.Map;

import io.restassured.response.Response;

public class ServiceUtil {
	private String locationUrl;
	private String getLocationPath;
	private String userKey;
	
	public ServiceUtil() {
		ApplicationPropertiesInitializer propertiesInitializer = new ApplicationPropertiesInitializer();
		locationUrl = propertiesInitializer.getLocationUrl();
		getLocationPath = propertiesInitializer.getGetLocationPath();
		userKey = propertiesInitializer.getUserKey();
	}
	
	public Response getLocationRequest(String query, double lat, double lng, int count) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, String> headerMap = new HashMap<String, String>();
		
		headerMap.put("Content-Type", "application/json");
		headerMap.put("user-key", userKey);
		
		paramMap.put("query", query);
		paramMap.put("lat", lat);
		paramMap.put("lon", lng);
		paramMap.put("count", count);

		RestGetRequestUtil restGetRequestUtil = new RestGetRequestUtil();
		Response response = restGetRequestUtil.httpGetAPICall(locationUrl, getLocationPath, paramMap, headerMap);

		return response;

	}
	
	public Response getLocationRequest(String query) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, String> headerMap = new HashMap<String, String>();
		
		headerMap.put("Content-Type", "application/json");
		headerMap.put("user-key", userKey);
		
		paramMap.put("query", query);

		RestGetRequestUtil restGetRequestUtil = new RestGetRequestUtil();
		Response response = restGetRequestUtil.httpGetAPICall(locationUrl, getLocationPath, paramMap, headerMap);

		return response;

	}
	
	public Response getLocationRequest(String query, String userKey) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, String> headerMap = new HashMap<String, String>();
		
		headerMap.put("Content-Type", "application/json");
		headerMap.put("user-key", userKey);
		
		paramMap.put("query", query);

		RestGetRequestUtil restGetRequestUtil = new RestGetRequestUtil();
		Response response = restGetRequestUtil.httpGetAPICall(locationUrl, getLocationPath, paramMap, headerMap);

		return response;

	}
	
	public Response getLocationRequest() {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, String> headerMap = new HashMap<String, String>();
		
		headerMap.put("Content-Type", "application/json");
		headerMap.put("user-key", userKey);

		RestGetRequestUtil restGetRequestUtil = new RestGetRequestUtil();
		Response response = restGetRequestUtil.httpGetAPICall(locationUrl, getLocationPath, paramMap, headerMap);

		return response;

	}




}
