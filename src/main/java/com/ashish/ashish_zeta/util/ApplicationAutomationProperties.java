package com.ashish.ashish_zeta.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import lombok.Data;

@Data
public class ApplicationAutomationProperties {

	private Properties properties;

	public ApplicationAutomationProperties(String propertiesFileName, String propertiesFilePath) {
		try {
			properties = new Properties();
			String filePath = propertiesFilePath + propertiesFileName;
			InputStream input;

			input = new FileInputStream(filePath);
			properties.load(input);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

