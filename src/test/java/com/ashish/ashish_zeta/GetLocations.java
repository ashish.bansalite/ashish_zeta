package com.ashish.ashish_zeta;


import org.testng.Assert;
import org.testng.annotations.Test;

import com.ashish.ashish_zeta.models.GetLocationsResponse;
import com.ashish.ashish_zeta.models.LocationSuggestions;
import com.ashish.ashish_zeta.util.ServiceUtil;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetLocations {
	
	@Test(description = "Test with all the params")
	public void testGetLocationsWithAllParam() {
		ServiceUtil serviceUtil = new ServiceUtil();
		Response response = serviceUtil.getLocationRequest("HSR", 12.914669, 77.632458, 1);
		GetLocationsResponse getLocationsResponse =  response.getBody().as(GetLocationsResponse.class);
		Assert.assertEquals(response.getStatusCode(), 200, "Incorrect status code returned");
		System.out.println("getLocationsResponse = " + getLocationsResponse);
		Assert.assertEquals(getLocationsResponse.getStatus(), "success");
		Assert.assertTrue(getLocationsResponse.getLocation_suggestions().size() > 0);
		Assert.assertTrue(getLocationsResponse.getLocation_suggestions().get(0).getTitle().contains("HSR"));
		Assert.assertTrue("India".equalsIgnoreCase(getLocationsResponse.getLocation_suggestions().get(0).getCountry_name()));
	}
	
	@Test(description = "Test with all the params, count is more than 1")
	public void testGetLocationsWithCount2() {
		ServiceUtil serviceUtil = new ServiceUtil();
		Response response = serviceUtil.getLocationRequest("HSR", 12.914669, 77.632458, 2);
		GetLocationsResponse getLocationsResponse =  response.getBody().as(GetLocationsResponse.class);
		Assert.assertEquals(response.getStatusCode(), 200, "Incorrect status code returned");
		System.out.println("getLocationsResponse = " + getLocationsResponse);
		Assert.assertEquals(getLocationsResponse.getStatus(), "success");
		Assert.assertTrue(getLocationsResponse.getLocation_suggestions().size() > 0 && getLocationsResponse.getLocation_suggestions().size() <= 2);
		for (LocationSuggestions locationSuggestions : getLocationsResponse.getLocation_suggestions()) {
			Assert.assertTrue(locationSuggestions.getTitle().contains("HSR"));
			Assert.assertTrue("India".equalsIgnoreCase(getLocationsResponse.getLocation_suggestions().get(0).getCountry_name()));
		}
	}
	
	
	@Test(description = "Test with only required params")
	public void testGetLocationsWithRequiredParams() {
		ServiceUtil serviceUtil = new ServiceUtil();
		//Only suggestion for location name query is passed
		Response response = serviceUtil.getLocationRequest("HSR");
		GetLocationsResponse getLocationsResponse =  response.getBody().as(GetLocationsResponse.class);
		Assert.assertEquals(response.getStatusCode(), 200, "Incorrect status code returned");
		Assert.assertEquals(getLocationsResponse.getStatus(), "success");
		Assert.assertTrue(getLocationsResponse.getLocation_suggestions().size() > 0);
		for (LocationSuggestions locationSuggestions : getLocationsResponse.getLocation_suggestions()) {
			Assert.assertTrue(locationSuggestions.getTitle().contains("HSR"));
			Assert.assertTrue("India".equalsIgnoreCase(getLocationsResponse.getLocation_suggestions().get(0).getCountry_name()));
		}
	}
	
	@Test(description = "Test with wrong user key")
	public void testGetLocationsWithWrongUserKey() {
		ServiceUtil serviceUtil = new ServiceUtil();
		//Pass wrong user key
		Response response = serviceUtil.getLocationRequest("HSR", "999aaaa38370660197734fca9099zz9");
		Assert.assertEquals(response.getStatusCode(), 403, "Incorrect status code returned");
		JsonPath jsonpath = response.jsonPath();
		int code = jsonpath.getInt("code");
		String status = jsonpath.get("status");
		String message = jsonpath.get("message");
		Assert.assertEquals(code, 403);
		Assert.assertEquals(status, "Forbidden");
		Assert.assertEquals(message, "Invalid API Key");
	}
	
	//Test without query should give http 400 response but it's currently giving http 200 success with random location.
	//Making it enabled false
	@Test(description = "Test without query", enabled = false)
	public void testGetLocationsWithoutQuery() {
		ServiceUtil serviceUtil = new ServiceUtil();
		Response response = serviceUtil.getLocationRequest();
		Assert.assertEquals(response.getStatusCode(), 400, "Incorrect status code returned");
		JsonPath jsonpath = response.jsonPath();
		//Message should be proper
		String message = jsonpath.get("message");
		Assert.assertEquals(message, "Query is required param");
	}

}
